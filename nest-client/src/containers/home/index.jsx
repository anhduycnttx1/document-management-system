import Link from 'next/link'
import { Button } from 'antd'
export default function Home() {
  return (
    <>
      <div className="flex justify-center">
        <div className="my-20 ">
          <div className="text-center">
            <div className="text-6xl font-bold">
              Welcome to{' '}
              <Link href="https://nextjs.org">
                <a className="text-blue-500">Next.js!</a>
              </Link>
            </div>
            <div className="my-10 text-xl font-medium">
              Get started by editing <code className="px-3 py-1 bg-gray-100 rounded">pages/index.tsx</code>
            </div>
          </div>
          <div className="grid grid-cols-2 gap-4">
            <Link href="https://nextjs.org/docs">
              <a>
                <div className="p-6 border rounded-md hover:bg-gray-100 ">
                  <h2 className="text-2xl font-bold">
                    Documentation{' '}
                    <Button shape="circle" className="text-white bg-blue-400">
                      {'>'}
                    </Button>
                  </h2>
                  <p className="mt-2 text-lg">Find in-depth information about Next.js features and API.</p>
                </div>
              </a>
            </Link>
            <Link href="https://nextjs.org/learn">
              <a>
                <div className="p-6 border rounded-md hover:bg-gray-100">
                  <h2 className="text-2xl font-bold">
                    Learn{' '}
                    <Button shape="circle" className="text-white bg-blue-400">
                      {'>'}
                    </Button>
                  </h2>
                  <p className="mt-2 text-lg">Learn about Next.js in an interactive course with quizzes!</p>
                </div>
              </a>
            </Link>
            <Link href="https://github.com/vercel/next.js/tree/canary/examples">
              <a className="p-6 border rounded-md hover:bg-gray-100">
                <h2 className="text-2xl font-bold">
                  Examples{' '}
                  <Button shape="circle" className="text-white bg-blue-400">
                    {'>'}
                  </Button>
                </h2>
                <p className="mt-2 text-lg">Discover and deploy boilerplate example Next.js projects.</p>
              </a>
            </Link>
            <Link href="https://vercel.com/new?utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app">
              <a className="p-6 border rounded-md hover:bg-gray-100">
                <h2 className="text-2xl font-bold">
                  Deploy{' '}
                  <Button shape="circle" className="text-white bg-blue-400">
                    {'>'}
                  </Button>
                </h2>
                <p className="mt-2 text-lg">Instantly deploy your Next.js site to a public URL with Vercel.</p>
              </a>
            </Link>
          </div>
        </div>
      </div>
    </>
  )
}
