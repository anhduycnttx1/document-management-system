import { namespaceConfig } from 'fast-redux'

export type homePageStoreProps = {}

const DEFAULT_STATE: homePageStoreProps = {}

const { actions, getState } = namespaceConfig('homePage', DEFAULT_STATE)

export const getRoot = (state: any): homePageStoreProps | undefined => getState(state)
