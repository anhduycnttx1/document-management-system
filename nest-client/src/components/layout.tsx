import Head from 'next/head'
import Image from 'next/image'
interface AppLayoutProps {}
export default function Layout({ children }: React.PropsWithChildren<AppLayoutProps>) {
  return (
    <>
      <Head>
        <title>Layouts Example</title>
      </Head>
      <main className="">
        {children}
        <footer className="absolute bottom-6">
          <a
            href="https://vercel.com?utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app"
            target="_blank"
            rel="noopener noreferrer"
          >
            Powered by{' '}
            <span className="">
              <Image src="/vercel.svg" alt="Vercel Logo" width={72} height={16} />
            </span>
          </a>
        </footer>
      </main>
    </>
  )
}
