import Link from 'next/link'

export default function Sidebar() {
  return (
    <nav className="flex justify-between w-full h-5">
      <input className="" placeholder="Search..." />
      <Link href="/">
        <a>Home</a>
      </Link>
      <Link href="/about">
        <a>About</a>
      </Link>
      <Link href="/contact">
        <a>Contact</a>
      </Link>
    </nav>
  )
}
