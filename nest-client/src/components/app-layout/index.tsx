import Head from 'next/head'
import React from 'react'
import Image from 'next/image'
import Link from 'next/link'

interface AppLayoutProps {
  showMenu?: boolean
  nav?: JSX.Element
  navWidth?: number | string
  searchBox?: JSX.Element
}

export default function AppLayout({ children }: React.PropsWithChildren<AppLayoutProps>) {
  return (
    <>
      <Head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
      </Head>
      <main className="">{children}</main>

      <footer className="absolute right-10 bottom-5">
        <Link href="https://vercel.com?utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app">
          <a>
            Powered by{' '}
            <span className="">
              <Image src="/vercel.svg" alt="Vercel Logo" width={72} height={16} />
            </span>
          </a>
        </Link>
      </footer>
    </>
  )
}
