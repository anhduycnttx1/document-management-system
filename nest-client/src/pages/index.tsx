import Head from 'next/head'
import HomeContainer from '../containers/home'
import AppLayout from '../components/app-layout'
import React from 'react'

export default function Page() {
  return (
    <>
      <Head>
        <title>Home App</title>
      </Head>
      <HomeContainer />
    </>
  )
}

Page.getLayout = (page: React.ReactElement) => {
  return <AppLayout>{page}</AppLayout>
}
